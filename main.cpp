#include <iostream>

using namespace std;

int main() {
    int counter;
    char id1, id2, in1, in2;
    int win1 = 0, win2 = 0, win3 = 0, lose1 = 0, lose2 = 0, lose3 = 0, draw1 = 0, draw2 = 0, draw3 = 0;
    int tmp;
    cin >> counter;
    while (counter--) {
        cin >> id1 >> in1 >> id2 >> in2;
        if (id1 == id2) {
            continue;
        }

        if (in1 == 'J' && (in2 == 'P' || in2 == 'S')) {
            tmp = 1;
        } else if (in1 == 'K' && (in2 == 'J' || in2 == 'N')) {
            tmp = 1;
        } else if (in1 == 'N' && (in2 == 'J' || in2 == 'P')) {
            tmp = 1;
        } else if (in1 == 'P' && (in2 == 'K' || in2 == 'S')) {
            tmp = 1;
        } else if (in1 == 'S' && (in2 == 'K' || in2 == 'N')) {
            tmp = 1;
        } else if (in1 == in2) {
            tmp = 0;
        } else {
            tmp = -1;
        }

        if (tmp == 1) {
            if (id1 == 'a') {
                win1++;
            }
            if (id1 == 'b') {
                win2++;
            }
            if (id1 == 'c') {
                win3++;
            }
            if (id2 == 'a') {
                lose1++;
            }
            if (id2 == 'b') {
                lose2++;
            }
            if (id2 == 'c') {
                lose3++;
            }
        }
        if (tmp == 0) {
            if (id1 == 'a') {
                draw1++;
            }
            if (id1 == 'b') {
                draw2++;
            }
            if (id1 == 'c') {
                draw3++;
            }
            if (id2 == 'a') {
                draw1++;
            }
            if (id2 == 'b') {
                draw2++;
            }
            if (id2 == 'c') {
                draw3++;
            }
        }
        if (tmp == -1) {
            if (id1 == 'a') {
                lose1++;
            }
            if (id1 == 'b') {
                lose2++;
            }
            if (id1 == 'c') {
                lose3++;
            }
            if (id2 == 'a') {
                win1++;
            }
            if (id2 == 'b') {
                win2++;
            }
            if (id2 == 'c') {
                win3++;
            }
        }
    }
    double sum = 0;
    if (win1 > 0 || lose1 > 0 || draw1 > 0) {
        sum = win1 + lose1 + draw1;
        cout << "gracz a";
        if (win1 > 0) {
            cout << "\n    wygrane: " << (win1 / sum) * 100 << "%";//cout<<endl<<win1<<"/"<<sum<<endl;
        }
        if (draw1 > 0) {
            cout << "\n    remisy: " << (draw1 / sum) * 100 << "%";//cout<<endl<<draw1<<"/"<<sum<<endl;
        }
        if (lose1 > 0) {
            cout << "\n    przegrane: " << (lose1 / sum) * 100 << "%";//cout<<endl<<lose1<<"/"<<sum<<endl;
        }
        cout << "\n";
    }
    if (win2 > 0 || lose2 > 0 || draw2 > 0) {
        if (sum > 0) {
            cout << endl;
        }
        sum = win2 + lose2 + draw2;
        cout << "gracz b";
        if (win2 > 0) {
            cout << "\n    wygrane: " << (win2 / sum) * 100 << "%";
        }
        if (draw2 > 0) {
            cout << "\n    remisy: " << (draw2 / sum) * 100 << "%";
        }
        if (lose2 > 0) {
            cout << "\n    przegrane: " << (lose2 / sum) * 100 << "%";
        }
        cout << "\n";
    }
    if (win3 > 0 || lose3 > 0 || draw3 > 0) {
        if (sum > 0) {
            cout << endl;
        }
        sum = win3 + lose3 + draw3;
        cout << "gracz c";
        if (win3 > 0) {
            cout << "\n    wygrane: " << (win3 / sum) * 100 << "%";//cout<<endl<<win3<<"/"<<sum<<endl;
        }
        if (draw3 > 0) {
            cout << "\n    remisy: " << (draw3 / sum) * 100 << "%";//cout<<endl<<draw3<<"/"<<sum<<endl;
        }
        if (lose3 > 0) {
            cout << "\n    przegrane: " << (lose3 / sum) * 100 << "%";//cout<<endl<<lose3<<"/"<<sum<<endl;
        }
        cout << "\n";
    }
    return 0;
}
